# Sank #

Command line utility to download subtitles for movies and series.  
Support download from [www.opensubtitles.org](http://www.opensubtitles.org) and from [www.thesubdb.com](http://www.thesubdb.com).

There are binaries version for Linux and OsX [here](https://bitbucket.org/ntfx/sank/downloads/).

## Usage ##

### CLI ###

Download english subtitles
```
sank download -d eng Underworld.Awakening.2012.720p.BrRip.x264.YIFY.mp4
```

Download *all* available english subtitles
```
sank download -a -d eng Underworld.Awakening.2012.720p.BrRip.x264.YIFY.mp4
```

### Library ###

```go
// calculate movie hash 
hash, err := utils.HashFile(filename)
// get file size
size, err := utils.FileSize(filename)

// build request params
param := []opensub.SearchHashIDReq{
    opensub.SearchHashIDReq{
        Hash:          fmt.Sprintf("%016x", hash),
        Size:          size,
        SubLanguageID: "eng",
    },
}

// search subtitle on opensubtitles.org
resp, err := osub.SearchByHashID(&param)

// get best subtitle. the one that has more downloads
bestIdx := resp.MoreDownloads()
data, err := download(osub, resp.data[bestIdx].IDSubtitleFile)

// extract subtitle (base64 encoded and gziped)
unboxed, err := utils.UnBox(data[0].Data, resp.data[bestIdx].SubEncoding)

```