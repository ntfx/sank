package main

import "bitbucket.com/ntfx/sank/cmd/sank/app"

func main() {
	app.Execute()
}
