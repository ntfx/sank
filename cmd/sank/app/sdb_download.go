package app

import (
	"fmt"
	"io/ioutil"

	"golang.org/x/text/encoding/htmlindex"

	"bytes"

	"bitbucket.com/ntfx/sank/clients/subdb"
	"bitbucket.com/ntfx/sank/utils"
	"github.com/saintfish/chardet"
	"github.com/spf13/cobra"
)

var dwnCmd = &cobra.Command{
	Use:   "download [filename]",
	Short: "Download subtitles",
	Long:  "Download subtitles from thesubdb.com",
	Run:   dwnCommand,
}

func init() {
	SdbCmd.AddCommand(dwnCmd)
	dwnCmd.Flags().StringVarP(&prm.LangID, "langid", "d", "", "language id (ex: es, en, pt, fr...)")
}

func dwnCommand(cmd *cobra.Command, args []string) {

	if len(args) == 0 {
		fmt.Println("Must speficy a movie filename (mp4, avi, mkv, etc)")
		return
	}

	s := subdb.New(subdb.APIurlSandBox, SdbUA)

	if prm.LangID == "" {
		fmt.Println("Must specify a language to download")
		fmt.Println("Available languages:")
		langs, err := s.Languages()
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println(string(langs))
		return
	}

	hash, err := utils.SdbMovieHash(args[0])
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	subs, err := s.Download(hash, prm.LangID)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	dtd := chardet.NewTextDetector()
	r, err := dtd.DetectBest(subs)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	strEncoder, err := htmlindex.Get(r.Charset)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	iReader := strEncoder.NewDecoder().Reader(bytes.NewBuffer(subs))
	outData, err := ioutil.ReadAll(iReader)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if err := SaveSubtitle(outData, args[0], -1, prm.LangID, "srt"); err != nil {
		fmt.Println(err.Error())
		return
	}

}
