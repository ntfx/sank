package app

import "github.com/spf13/cobra"

// SdbUA is the UserAgent string for Sank
var SdbUA = "SubDB/1.0 (Sank/0.1; https://bitbucket.org/ntfx/sank)"

// SdbCmd is the namespace for SubDB commands
var SdbCmd = &cobra.Command{
	Use:   "sdb",
	Short: "thesubdb.com commands",
	Long:  `Commands show here are to work with thesubdb.com`,
	Run:   sdbCommand,
}

func init() {
	RootCmd.AddCommand(SdbCmd)
}

func sdbCommand(cmd *cobra.Command, args []string) {
	cmd.Help()
}
