package app

import (
	"fmt"

	"bitbucket.com/ntfx/sank/clients/subdb"
	"bitbucket.com/ntfx/sank/utils"
	"github.com/spf13/cobra"
)

var sdbSearchCmd = &cobra.Command{
	Use:   "search [filename]",
	Short: "Search for subtitles",
	Long:  "Search for subtitles using the hash movie",
	Run:   sdbSearchCommand,
}

func init() {
	SdbCmd.AddCommand(sdbSearchCmd)
}

func sdbSearchCommand(cmd *cobra.Command, args []string) {

	if len(args) == 0 {
		fmt.Println("Must speficy a movie filename (mp4, avi, mkv, etc)")
		return
	}

	s := subdb.New(subdb.APIurlSandBox, SdbUA)
	hash, err := utils.SdbMovieHash(args[0])
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	subs, err := s.Search(hash)

	fmt.Println(string(subs))

}
