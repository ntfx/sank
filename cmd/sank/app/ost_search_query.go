package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"bitbucket.com/ntfx/sank/clients/opensub"
	"github.com/spf13/cobra"
)

var searchCmd = &cobra.Command{
	Use:   "search {string}",
	Short: "Search OpenSubtitles text",
	Long:  `Search OpenSubtitles using text and limit using some parameters.`,
	RunE:  searchCommand,
}

func init() {
	OstCmd.AddCommand(searchCmd)
	searchCmd.Flags().StringVarP(&prm.LangID, "langid", "d", "", "language id (ex: spa, eng, rum, ara...)")
	searchCmd.Flags().StringVarP(&prm.Season, "season", "s", "", "season number")
	searchCmd.Flags().StringVarP(&prm.Episode, "episode", "e", "", "episode number")
}

func searchCommand(cmd *cobra.Command, args []string) error {
	if len(args) == 0 {
		return errors.New("Error: Missing parameters")
	}

	os, err := OSSession()
	if err != nil {
		return err
	}

	req := []opensub.SearchQueryReq{
		opensub.SearchQueryReq{
			Query:         args[0],
			SubLanguageID: prm.LangID,
			Season:        prm.Season,
			Episode:       prm.Episode,
		},
	}

	res, err := os.SearchByQuery(&req)
	if err != nil {
		return err
	}

	if Config.JSON {
		js, err := json.Marshal(res)
		if err != nil {
			return err
		}
		fmt.Printf("%s\n", js)
		return nil
	}

	DumpReflect(reflect.ValueOf(res), "")

	return nil
}
