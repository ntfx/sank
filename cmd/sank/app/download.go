package app

import (
	"fmt"
	"io/ioutil"
	"path"
	"strconv"
	"strings"

	"bitbucket.com/ntfx/sank/clients/opensub"
	"bitbucket.com/ntfx/sank/utils"

	"github.com/spf13/cobra"
)

var downloadCmd = &cobra.Command{
	Use:   "download [movie_file_name]",
	Short: "Download subtitles",
	Long:  `Download subtitles from OpenSubtitles`,
	Run:   downloadCommand,
}

func init() {
	RootCmd.AddCommand(downloadCmd)
	downloadCmd.Flags().StringVarP(&prm.SubtitleID, "subid", "s", "", "SubtitleID to download")
	downloadCmd.Flags().StringVarP(&prm.LangID, "langid", "d", "", "language id (ex: spa, eng, rum, ara...)")
	downloadCmd.Flags().StringVarP(&prm.Outputfile, "output", "o", "", "output file name")
	downloadCmd.Flags().BoolVarP(&prm.AllFiles, "all", "a", false, "get all available subtitles")
}

func downloadCommand(cmd *cobra.Command, args []string) {

	if (prm.SubtitleID == "") && (len(args) == 0) {
		fmt.Printf("Error: Need to specify some file to download\n")
		cmd.Help()
		return
	}

	if prm.LangID == "" {
		fmt.Printf("Error: Need to specify subtitle language\n")
		cmd.Help()
		return
	}

	osub, err := OSSession()
	if err != nil {
		fmt.Printf(err.Error())
		return
	}

	if prm.SubtitleID != "" {
		if err := subtitlesByID(osub); err != nil {
			fmt.Printf(err.Error())
			return
		}
	}

	if len(args) > 0 {
		if err := subtitlesByName(osub, args[0]); err != nil {
			fmt.Printf(err.Error())
			return
		}
	}
}

func subtitlesByID(osub *opensub.Client) error {

	subs := strings.Split(prm.SubtitleID, ",")
	res, err := osub.DownloadSubtitles(&subs)
	if err != nil {
		fmt.Printf("Error downloading subtitle: %s\n", err.Error())
		return err
	}

	var filename string
	for i, x := range res.Data {

		unboxed, err := utils.UnBox(x.Data, "")
		if err != nil {
			fmt.Printf("Error decoding subtitle %d ~ %s\n", i, err.Error())
			continue
		}

		if prm.Outputfile != "" {
			if i > 0 {
				extName := path.Ext(prm.Outputfile)
				baseName := prm.Outputfile[0 : len(prm.Outputfile)-len(extName)]
				filename = baseName + strconv.FormatInt(int64(i), 10) + extName
			} else {
				filename = prm.Outputfile
			}
		} else {
			filename = x.SubtitleID + ".srt"
		}

		if err := ioutil.WriteFile(filename, unboxed, 0644); err != nil {
			fmt.Printf("Error saving subtitle %d ~ %s\n", i, err.Error())
			continue
		}
	}

	return nil
}

func subtitlesByName(osub *opensub.Client, filename string) error {

	hash, err := utils.HashFile(filename)
	if err != nil {
		return err
	}

	size, err := utils.FileSize(filename)
	if err != nil {
		return err
	}

	param := []opensub.SearchHashIDReq{
		opensub.SearchHashIDReq{
			Hash:          fmt.Sprintf("%016x", hash),
			Size:          size,
			SubLanguageID: prm.LangID,
		},
	}

	resp, err := osub.SearchByHashID(&param)
	if err != nil {
		return err
	}

	if len(resp.Data) == 0 {
		fmt.Println("No subtitles found.")
		return nil
	}

	if prm.AllFiles {
		for i, x := range resp.Data {
			processSubtitle(osub, x, filename, i)
		}
	} else {
		bestIdx, err := resp.MoreDownloads()
		if err != nil {
			return err
		}
		processSubtitle(osub, resp.Data[bestIdx], filename, -1)
	}

	return nil
}

func processSubtitle(osub *opensub.Client, st opensub.SearchSubtitle, filename string, idx int) {
	data, err := download(osub, st.IDSubtitleFile)
	if err != nil {
		fmt.Printf("Error downloading subtitle for %s ~ %s", st.MovieName, err.Error())
	} else {
		unboxed, err := utils.UnBox(data[0].Data, st.SubEncoding)
		if err != nil {
			fmt.Printf("Error decoding subtitle for %s ~ %s", st.MovieName, err.Error())
		} else {
			SaveSubtitle(unboxed, filename, idx, st.SubLanguageID, st.SubFormat)
		}
	}
}

func download(osub *opensub.Client, subid string) ([]opensub.SubtitleData, error) {
	subs := []string{subid}
	res, err := osub.DownloadSubtitles(&subs)
	if err != nil {
		return nil, err
	}
	return res.Data, nil
}
