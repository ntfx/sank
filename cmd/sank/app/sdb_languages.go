package app

import (
	"fmt"

	"bitbucket.com/ntfx/sank/clients/subdb"
	"github.com/spf13/cobra"
)

var langCmd = &cobra.Command{
	Use:   "languages",
	Short: "List available laguages",
	Long:  "List available laguages to request subtitles",
	Run:   langCommand,
}

func init() {
	SdbCmd.AddCommand(langCmd)
}

func langCommand(cmd *cobra.Command, args []string) {

	s := subdb.New(subdb.APIurlSandBox, SdbUA)
	langs, err := s.Languages()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(string(langs))
}
