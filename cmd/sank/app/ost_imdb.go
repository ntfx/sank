package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"bitbucket.com/ntfx/sank/clients/opensub"

	"github.com/spf13/cobra"
)

var imdbCmd = &cobra.Command{
	Use:   "imdb {imdbid}",
	Short: "Search OpenSubtitles DB by IMDB ID",
	Long:  `Search OpenSubtitles using IMDB ID identifier`,
	RunE:  imdbCommand,
}

func init() {
	OstCmd.AddCommand(imdbCmd)
	imdbCmd.Flags().StringVarP(&prm.LangID, "langid", "d", "", "language id (ex: spa, eng, rum, ara...)")
	imdbCmd.Flags().StringVarP(&prm.Season, "season", "s", "", "season number")
	imdbCmd.Flags().StringVarP(&prm.Episode, "episode", "e", "", "episode number")
	imdbCmd.Flags().StringVarP(&prm.Tags, "tags", "t", "", "tags (ex: dvd, hdtv, tv, vod, web-dl, fleet, axxo, killers...")
}

func imdbCommand(cmd *cobra.Command, args []string) error {

	if len(args) == 0 {
		return errors.New("Error: Missing parameters")
	}

	os, err := OSSession()
	if err != nil {
		return err
	}

	var req = []opensub.SearchIMDBReq{
		opensub.SearchIMDBReq{
			ImdbID:        args[0],
			SubLanguageID: prm.LangID,
			Season:        prm.Season,
			Episode:       prm.Episode,
			Tags:          prm.Tags,
		},
	}

	res, err := os.SearchByIMDB(&req)
	if err != nil {
		return err
	}

	if Config.JSON {
		js, err := json.Marshal(res)
		if err != nil {
			return err
		}
		fmt.Printf("%s\n", js)
		return nil
	}

	DumpReflect(reflect.ValueOf(res), "")

	return nil
}
