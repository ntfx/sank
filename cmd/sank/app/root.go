package app

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"reflect"
	"strconv"

	"bitbucket.com/ntfx/sank/clients/opensub"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// UA is the UserAgent used for OpenSubtitles
var UA = "sank 0.1"

// Config variables for CLI
var Config struct {
	ConfigFile string
	Verbose    bool
	Username   string
	Password   string
	Language   string
	JSON       bool
}

var prm struct {
	ImdbID     string
	LangID     string
	Season     string
	Episode    string
	Tags       string
	SubtitleID string
	Filename   string
	Outputfile string
	AllFiles   bool
}

// RootCmd base command without subcommands
var RootCmd = &cobra.Command{
	Use:   "sank",
	Short: "Subtitles manager",
	Long: `Utility to manage subtitles from different providers.
The basic functionality is search and download subtitles but on some cases
can handle upload de new subtitles or discover new movies.`,
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		os.Exit(2)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().BoolVarP(&Config.Verbose, "verbose", "v", false, "verbose output")
	RootCmd.PersistentFlags().StringVarP(&Config.Username, "username", "u", "", "Username to log-in")
	RootCmd.PersistentFlags().StringVarP(&Config.Password, "password", "p", "", "Password to log-in")
	RootCmd.PersistentFlags().StringVarP(&Config.Language, "language", "l", "", "Default language")
	RootCmd.PersistentFlags().BoolVarP(&Config.JSON, "json", "j", false, "JSON output")
	RootCmd.PersistentFlags().StringVar(&Config.ConfigFile, "config", "", "config file (default is $HOME/.sank.yaml)")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if Config.ConfigFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(Config.ConfigFile)
	}

	viper.SetConfigName(".sank") // name of config file (without extension)
	viper.AddConfigPath("$HOME") // adding home directory as first search path
	viper.AutomaticEnv()         // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

// OSSession create a new session with OpenSubtitles
func OSSession() (*opensub.Client, error) {

	os, err := opensub.New(opensub.APIurl, UA)
	if err != nil {
		return nil, err
	}

	if err := os.Login(Config.Username, Config.Password, Config.Language); err != nil {
		return nil, fmt.Errorf("Can not login: %s", err.Error())
	}

	return os, nil
}

// DumpReflect dump recursibly to stdout the content of a variable
func DumpReflect(data reflect.Value, tab string) reflect.Value {

	switch data.Kind() {
	case reflect.Ptr:
		DumpReflect(data.Elem(), tab)
	case reflect.Struct:
		x := data.NumField()
		for i := 0; i < x; i++ {
			v := data.Field(i)
			fmt.Printf("%s%s: ", tab, data.Type().Field(i).Name)
			fmt.Printf("%v\n", DumpReflect(v, tab+"\t"))
		}
		return reflect.ValueOf(" ")
	case reflect.String:
		return data
	case reflect.Float32:
		return data
	case reflect.Slice:
		x := data.Len()
		for i := 0; i < x; i++ {
			fmt.Printf("%s%v\n", tab, DumpReflect(data.Index(i), tab+"\t"))
		}
		return reflect.ValueOf(" ")
	case reflect.Map:
		for i, v := range data.MapKeys() {
			fmt.Printf("%s%d: \n", tab, i)
			DumpReflect(data.MapIndex(v), tab+"\t")
		}
		return reflect.ValueOf(" ")
	default:
		fmt.Printf("> not found: type: %v ~ %v\n", data, data.Kind().String())
	}

	return reflect.ValueOf("#")
}

// SaveSubtitle store the subtitles in the filesystem
func SaveSubtitle(data []byte, filename string, idx int, lang string, fmt string) error {

	_, file := path.Split(filename)
	ext := path.Ext(filename)
	baseName := file[0 : len(file)-len(ext)]
	subFilename := ""
	if idx >= 0 {
		subFilename = baseName + "." + lang + "." + strconv.FormatInt(int64(idx), 10) + "." + fmt
	} else {
		subFilename = baseName + "." + lang + "." + fmt
	}

	if err := ioutil.WriteFile(subFilename, data, 0644); err != nil {
		return err
	}

	return nil
}
