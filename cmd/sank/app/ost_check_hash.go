package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"github.com/spf13/cobra"
)

var check2 bool

var checkHashCmd = &cobra.Command{
	Use:   "checkhash [hash]",
	Short: "Check the movie hash",
	Long:  `Verify the movie hash with the provider`,
	RunE:  checkHashCommand,
}

func init() {
	OstCmd.AddCommand(checkHashCmd)
	checkHashCmd.Flags().BoolVarP(&check2, "two", "2", false, "use version 2")
}

func checkHashCommand(cmd *cobra.Command, args []string) error {

	if len(args) == 0 {
		cmd.Help()
		return errors.New("Error: Missing parameters")
	}

	os, err := OSSession()
	if err != nil {
		return err
	}

	var res interface{}

	if check2 {
		res, err = os.CheckMovieHash2(&args)
	} else {
		res, err = os.CheckMovieHash(&args)
	}

	if err != nil {
		return err
	}

	if Config.JSON {
		js, err := json.Marshal(res)
		if err != nil {
			return err
		}
		fmt.Printf("%s\n", js)
		return nil
	}

	DumpReflect(reflect.ValueOf(res), "")

	return nil
}
