package app

import "github.com/spf13/cobra"

// OstCmd is the namespace for OpenSubtitles commands
var OstCmd = &cobra.Command{
	Use:   "ost",
	Short: "opensubtitles.org commands",
	Long:  `Commands show here are to work with OpenSubtitles.org`,
	Run:   ostCommand,
}

func init() {
	RootCmd.AddCommand(OstCmd)
}

func ostCommand(cmd *cobra.Command, args []string) {
	cmd.Help()
}
