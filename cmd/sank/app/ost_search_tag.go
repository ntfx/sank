package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"bitbucket.com/ntfx/sank/clients/opensub"
	"github.com/spf13/cobra"
)

var searchTagCmd = &cobra.Command{
	Use:   "tag {string}",
	Short: "Search OpenSubtitles DB tag",
	Long: `Search OpenSubtitles using a tag. Tag is index of movie filename
			or subtitle file name, or release name`,
	RunE: searchTagCommand,
}

func init() {
	OstCmd.AddCommand(searchTagCmd)
	searchTagCmd.Flags().StringVarP(&prm.LangID, "langid", "d", "", "language id (ex: spa, eng, rum, ara...)")
	searchTagCmd.Flags().StringVarP(&prm.ImdbID, "imdbid", "i", "", "IMDB ID")
}

func searchTagCommand(cmd *cobra.Command, args []string) error {

	if len(args) == 0 {
		return errors.New("Error: Missing parameters")
	}

	os, err := OSSession()
	if err != nil {
		return err
	}

	req := []opensub.SearchTagReq{
		opensub.SearchTagReq{
			Tag:           args[0],
			ImdbID:        prm.ImdbID,
			SubLanguageID: prm.LangID,
		},
	}

	res, err := os.SearchByTag(&req)
	if err != nil {
		return err
	}

	if Config.JSON {
		js, err := json.Marshal(res)
		if err != nil {
			return err
		}
		fmt.Printf("%s\n", js)
		return nil
	}

	DumpReflect(reflect.ValueOf(res), "")

	return nil
}
