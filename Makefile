VERSION := $(shell git describe --always --dirty)

help:
	@echo 'make build   - build the clipper executable'
	@echo 'make tag     - tag the current HEAD with VERSION'
	@echo 'make archive - create an archive of the current HEAD for VERSION'
	@echo 'make upload  - upload the built archive of VERSION to Amazon S3'
	@echo 'make all     - build, tag, archive and upload VERSION'

version:
	@if [ "$$VERSION" = "" ]; then echo "VERSION not set"; exit 1; fi

sank_linux:
	GOOS=linux GOARCH=amd64 go build -ldflags="-X main.version=${VERSION}" -o sank_linux cmd/sank/main.go

sank_darwin:
	GOOS=darwin GOARCH=amd64 go build -ldflags="-X main.version=${VERSION}" -o sank_darwin cmd/sank/main.go

sank_all: sank_linux sank_darwin

sank: cmd/sank/main.go
	go build -o sank -ldflags="-X main.version=${VERSION}" $^

build: sank

tag: version
	git tag -s $$VERSION -m "$$VERSION release"

archive: sank-${VERSION}.zip

sank-${VERSION}.zip: sank
	git archive -o $@ HEAD
	zip $@ sank

all: tag build archive upload

.PHONY: clean
clean:
	rm -f sank sank-*.zip
	rm -f sank_*
