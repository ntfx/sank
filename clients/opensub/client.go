package opensub

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"

	"github.com/kolo/xmlrpc"
)

// APIurl is the OpenSubtitles url for the xml-rpc requests
const APIurl = "http://api.opensubtitles.org/xml-rpc"
const resSuccess = "200 OK"
const resNoSession = "406 No session"

// ErrorNoSession is returned when the session for the current request has expired
type ErrorNoSession struct {
	error
}

// New create a new OpenSubtitles client to request data.
// If the package is used as a library for your application,
// you **MUST** request a User Agent string with OpenSubititles,
// for that check this
//		url: http://trac.opensubtitles.org/projects/opensubtitles/wiki/DevReadFirst
//
func New(apiURL, ua string) (*Client, error) {

	if (apiURL == "") || (ua == "") {
		return nil, errors.New("Invalid parameters")
	}

	rpc, err := xmlrpc.NewClient(apiURL, nil)
	if err != nil {
		return nil, err
	}

	return &Client{
		UserAgent: ua,
		Client:    rpc,
	}, nil
}

// Login into OpenSubtitles service
func (c *Client) Login(user, pass, lang string) error {
	c.UserName = user
	c.Password = pass
	c.Language = lang

	res := rpcLoginRes{}
	if err := c.Call("LogIn", []interface{}{c.UserName, c.Password, c.Language, c.UserAgent}, &res); err != nil {
		return err
	}

	if res.Status != resSuccess {
		return fmt.Errorf("Login Error: %s", res.Status)
	}
	c.Token = res.Token

	return nil
}

// Logout disconnect from OpenSubtitles
func (c *Client) Logout() error {

	res := rpcBaseRes{}
	if err := c.Call("LogOut", []interface{}{c.Token}, &res); err != nil {
		return err
	}

	if res.Status != resSuccess {
		return fmt.Errorf("Logout Error: %s", res.Status)
	}
	c.Token = ""

	return nil
}

// Noop is used to keep the session alive. The default session time is 15 minutes
func (c *Client) Noop() error {

	res := rpcBaseRes{}
	if err := c.Call("NoOperation", []interface{}{c.Token}, &res); err != nil {
		return err
	}

	if res.Status != resSuccess {
		if res.Status == resNoSession {
			return ErrorNoSession{errors.New("No Session")}
		}
		return fmt.Errorf("NoOperation Error: %s", res.Status)
	}

	return nil
}

// CheckSubHash check if the subtitles existes on the database using a special
// hash algorithm
//
// reference http://trac.opensubtitles.org/projects/opensubtitles/wiki/HashSourceCodes
//
func (c *Client) CheckSubHash(hashs *[]string) (*SubHashRes, error) {
	res := rpcBaseRes{}
	if err := c.Call("CheckSubHash", []interface{}{c.Token, hashs}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("CheckSubHash Error: %s", res.Status)
	}

	sub := SubHashRes{
		Status:  res.Status,
		Data:    make(map[string]string),
		Seconds: res.Seconds,
	}

	for k, v := range res.Data {
		switch t := v.(type) {
		case string:
			sub.Data[k] = t
		case int64:
			sub.Data[k] = strconv.FormatInt(t, 10)
		}
	}

	return &sub, nil
}

// CheckMovieHash returns best matching !MovieImdbID, MovieName, MovieYear,
// if available for each $moviehash. See also CheckMovieHash2().
// Read more about Movie Identification (http://trac.opensubtitles.org/projects/opensubtitles/wiki/MovieIdentification).
//
// Note: method accepts only first 200 hashes to avoid database load, not
// processed hashes are included in "not_processed".
// This method is similar to CheckMovieHash2 - it uses same SQL query,
// but prints just best guesses.
func (c *Client) CheckMovieHash(hashs *[]string) (*MovieHashRes, error) {

	res := struct {
		Status      string                 `xmlrpc:"status"`
		Data        map[string]interface{} `xmlrpc:"data"`
		NoProcessed []interface{}          `xmlrpc:"not_processed"`
		Seconds     float32                `xmlrpc:"seconds"`
	}{}

	if err := c.Call("CheckMovieHash", []interface{}{c.Token, hashs}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("CheckSubHash Error: %s", res.Status)
	}

	mh := MovieHashRes{
		Status:  res.Status,
		Data:    make(map[string]MovieHash),
		Seconds: res.Seconds,
	}

	// The results came on an interface field, need to extract it and move
	// to the definitive struct
	for k, v := range res.Data {
		r := reflect.ValueOf(v)
		switch r.Kind() {
		case reflect.Slice:
			// If the result is an Slice, the item is empty. The api return a different
			// type when the result is not found :(
		case reflect.Map:
			y := v.(map[string]interface{})
			mh.Data[k] = MovieHash{
				MovieHash:     y["MovieHash"].(string),
				MovieImdbID:   y["MovieImdbID"].(string),
				MovieName:     y["MovieName"].(string),
				MovieYear:     y["MovieYear"].(string),
				MovieKind:     y["MovieKind"].(string),
				SeriesSeason:  y["SeriesSeason"].(string),
				SeriesEpisode: y["SeriesEpisode"].(string),
				SeenCount:     y["SeenCount"].(string),
				SubCount:      y["SubCount"].(string),
			}
		}
	}

	return &mh, nil
}

// CheckMovieHash2 returns matching !MovieImdbID, MovieName, MovieYear,
// SeriesSeason, SeriesEpisode, MovieKind if available for each $moviehash,
// always sorted by MovieHash ASC, SubCount DESC, SeenCount DESC.
// Read more about Movie Identification (http://trac.opensubtitles.org/projects/opensubtitles/wiki/MovieIdentification).
func (c *Client) CheckMovieHash2(hashs *[]string) (*MovieHash2Res, error) {
	res := struct {
		Status  string                 `xmlrpc:"status"`
		Data    map[string]interface{} `xmlrpc:"data"`
		Seconds float32                `xmlrpc:"seconds"`
	}{}
	if err := c.Call("CheckMovieHash2", []interface{}{c.Token, hashs}, &res); err != nil {
		return nil, err
	}

	mh := MovieHash2Res{
		Status:  res.Status,
		Data:    make(map[string][]MovieHash),
		Seconds: res.Seconds,
	}

	// The results came on an interface field, need to extract it and move
	// to the definitive struct
	for k, v := range res.Data {
		x := make([]MovieHash, len(v.([]interface{})), len(v.([]interface{})))
		// If the result is an Slice, the item is empty. The api return a different
		// type when the result is not found :(

		for idx, g := range v.([]interface{}) {
			y := g.(map[string]interface{})
			x[idx].MovieHash = y["MovieHash"].(string)
			x[idx].MovieImdbID = y["MovieImdbID"].(string)
			x[idx].MovieName = y["MovieName"].(string)
			x[idx].MovieYear = y["MovieYear"].(string)
			x[idx].MovieKind = y["MovieKind"].(string)
			x[idx].SeriesSeason = y["SeriesSeason"].(string)
			x[idx].SeriesEpisode = y["SeriesEpisode"].(string)
			x[idx].SeenCount = y["SeenCount"].(string)
			x[idx].SubCount = y["SubCount"].(string)
		}
		mh.Data[k] = x
	}

	return &mh, nil
}

// DownloadSubtitles returns BASE64 encoded gzipped IDSubtitleFile(s).
// LIMIT is for maximum 20 IDSubtitleFiles, others will be ignored.
func (c *Client) DownloadSubtitles(subids *[]string) (*SubtitlesDataRes, error) {
	res := SubtitlesDataRes{}
	if err := c.Call("DownloadSubtitles", []interface{}{c.Token, subids}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("DownloadSubtitles Error: %s", res.Status)
	}

	return &res, nil
}

// SearchByHashID request the search using the Hash of the file and the byte
// size information.
// For more information check http://trac.opensubtitles.org/projects/opensubtitles/wiki/XMLRPC#SearchSubtitles
func (c *Client) SearchByHashID(hashids *[]SearchHashIDReq) (*SearchSubtitleRes, error) {
	res := SearchSubtitleRes{}
	if err := c.Call("SearchSubtitles", []interface{}{c.Token, hashids}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("SearchByHashID Error: %s", res.Status)
	}

	return &res, nil
}

// SearchByIMDB request the search using the IMDB id.
// For more information check http://trac.opensubtitles.org/projects/opensubtitles/wiki/XMLRPC#SearchSubtitles
func (c *Client) SearchByIMDB(hashids *[]SearchIMDBReq) (*SearchSubtitleRes, error) {
	res := SearchSubtitleRes{}
	if err := c.Call("SearchSubtitles", []interface{}{c.Token, hashids}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("SearchByIMDB Error: %s", res.Status)
	}

	return &res, nil
}

// SearchByTag request the search using a tag.
// Tag is index of movie filename or subtitle file name, or release name
// For more information check http://trac.opensubtitles.org/projects/opensubtitles/wiki/XMLRPC#SearchSubtitles
func (c *Client) SearchByTag(tag *[]SearchTagReq) (*SearchSubtitleRes, error) {
	res := SearchSubtitleRes{}
	if err := c.Call("SearchSubtitles", []interface{}{c.Token, tag}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("SearchByTag Error: %s", res.Status)
	}

	return &res, nil
}

// SearchByQuery request the search by an arbitrary text and can filter
// for some fields too.
// For more information check http://trac.opensubtitles.org/projects/opensubtitles/wiki/XMLRPC#SearchSubtitles
func (c *Client) SearchByQuery(qry *[]SearchQueryReq) (*SearchSubtitleRes, error) {
	res := SearchSubtitleRes{}
	if err := c.Call("SearchSubtitles", []interface{}{c.Token, qry}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("SearchByQuery Error: %s", res.Status)
	}

	return &res, nil
}

// GetSubLanguages returns list of allowed subtitle languages, default is english language
func (c *Client) GetSubLanguages(lang string) (*SubLanguageRes, error) {
	res := SubLanguageRes{}
	if err := c.Call("GetSubLanguages", []interface{}{lang}, &res); err != nil {
		return nil, err
	}

	// This response don't return status

	return &res, nil
}

// GetUserInfo returns info about logged-in user.
func (c *Client) GetUserInfo() (*GetUserInfoRes, error) {
	res := GetUserInfoRes{}

	if err := c.Call("GetUserInfo", []interface{}{c.Token}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("GetUserInfo Error: %s", res.Status)
	}

	return &res, nil
}

// PreviewSubtitles returns BASE64 encoded gzipped preview for IDSubtitleFile(s)
func (c *Client) PreviewSubtitles(subids *[]string) (*SubtitlePreviewRes, error) {

	res := SubtitlePreviewRes{}
	if err := c.Call("PreviewSubtitles", []interface{}{c.Token, subids}, &res); err != nil {
		return nil, err
	}

	if res.Status != resSuccess {
		return nil, fmt.Errorf("PreviewSubtitles Error: %s", res.Status)
	}

	return &res, nil

}

/*

ServerInfo
* LogIn
* LogOut
* SearchSubtitles
SearchToMail
* CheckSubHash
* CheckMovieHash
* CheckMovieHash2
InsertMovieHash
TryUploadSubtitles
UploadSubtitles
DetectLanguage
* PreviewSubtitles
* DownloadSubtitles
ReportWrongMovieHash
ReportWrongImdbMovie
* GetSubLanguages
GetAvailableTranslations
GetTranslation
SearchMoviesOnIMDB
GetIMDBMovieDetails
InsertMovie
SubtitlesVote
GetComments
AddComment
AddRequest
SetSubscribeUrl
SubscribeToHash
QuickSuggest
SuggestMovie
* GetUserInfo
GuessMovieFromString
AutoUpdate
* NoOperation

*/
