package opensub

import (
	"os"
	"testing"

	"bitbucket.com/ntfx/sank/clients/opensub"
)

const osTestUA = "OSTestUserAgentTemp"

var client *opensub.Client

func TestMain(m *testing.M) {
	var err error
	client, err = doDummyLogin()
	if err == nil {
		os.Exit(m.Run())
	}
}

func doDummyLogin() (*opensub.Client, error) {
	c, err := opensub.New(opensub.APIurl, osTestUA)
	if err != nil {
		return nil, err
	}

	err = c.Login("dummy", "dummy", "eng")
	if err != nil {
		return nil, err
	}

	return c, nil
}

func TestMissingParameters(t *testing.T) {
	_, err := opensub.New("", "")
	if err == nil {
		t.Fatal("Must require api and user agent")
	}

	_, err = opensub.New("dummy", "")
	if err == nil {
		t.Fatal("Must require user agent")
	}

	_, err = opensub.New("", "dummy")
	if err == nil {
		t.Fatal("Must require api url")
	}
}

func TestSetUserUA(t *testing.T) {
	userUA := "myUA"
	c, err := opensub.New(opensub.APIurl, userUA)
	if err != nil {
		t.Fatal(err)
	}
	if c.UserAgent != userUA {
		t.Fail()
	}
}

func TestLogin(t *testing.T) {
	c, err := opensub.New(opensub.APIurl, osTestUA)
	if err != nil {
		t.Fatal(err)
	}

	err = c.Login("asdfkljhsdf", "qwerpoiuwer", "234")
	if err == nil {
		t.Fatal("Must use invalid credentials")
	}

	err = c.Login("dummy", "dummy", "eng")
	if err != nil {
		t.Fatal("Invalid credentials")
	}

}

func TestLogout(t *testing.T) {

	c, err := doDummyLogin()
	if err != nil {
		t.Fatal(err)
	}
	err = c.Logout()
	if err != nil {
		t.Fatal(err)
	}
}

func TestNoop(t *testing.T) {

	c, err := doDummyLogin()
	if err != nil {
		t.Fatal(err)
	}

	if err := c.Noop(); err != nil {
		t.Fatal(err)
	}

	if err := c.Logout(); err != nil {
		t.Fatal(err)
	}

	if err := c.Noop(); err != nil {
		if err != err.(opensub.ErrorNoSession) {
			t.Fatal(err)
		}
	}

}

func TestCheckSubHash(t *testing.T) {

	hsh := []string{
		"a9672c89bc3f5438f820f06bab708067",
		"0ca1f1e42cfb58c1345e149f98ac3aec",
		"11111111111111111111111111111111",
	}

	res, err := client.CheckSubHash(&hsh)
	if err != nil {
		t.Fatal(err)
	}

	if len(res.Data) != 3 {
		t.Fatal("Result size mistmatch")
	}

}

func TestCheckMovieHash(t *testing.T) {

	hsh := []string{
		"46e33be00464c12e",
		"53fc6fe84ad5ee31",
		"abcdefg123211222",
	}

	res, err := client.CheckMovieHash(&hsh)
	if err != nil {
		t.Fatal(err)
	}

	// only return the matched results
	if len(res.Data) != 2 {
		t.Fatal("Result size mistmatch")
	}
}

func TestCheckMovieHash2(t *testing.T) {

	hsh := []string{
		"46e33be00464c12e",
		"53fc6fe84ad5ee31",
		"abcdefg123211222",
	}

	res, err := client.CheckMovieHash2(&hsh)
	if err != nil {
		t.Fatal(err)
	}

	// only return the matched results
	if len(res.Data) != 2 {
		t.Fatal("Result size mistmatch")
	}
}

func TestDownloadSubtitles(t *testing.T) {

	subids := []string{"1954677189"}

	res, err := client.DownloadSubtitles(&subids)
	if err != nil {
		t.Fatal(err)
	}

	// only return the matched results
	if len(res.Data) != 1 {
		t.Fatal("Result size mistmatch")
	}

}

func TestSearchByHashID(t *testing.T) {

	ids := []opensub.SearchHashIDReq{
		opensub.SearchHashIDReq{
			Hash: "18379ac9af039390",
			Size: 366876694,
		},
	}

	search, err := client.SearchByHashID(&ids)
	if err != nil {
		t.Fatal(err)
	}

	if len(search.Data) != 88 {
		t.Fatal("Result size mistmatch")
	}
}

func TestSearchByIMDB(t *testing.T) {

	ids := []opensub.SearchIMDBReq{
		opensub.SearchIMDBReq{
			ImdbID:        "0475784",
			SubLanguageID: "eng",
			Season:        "1",
			Episode:       "1",
		},
		opensub.SearchIMDBReq{
			ImdbID:        "2908446",
			SubLanguageID: "eng",
		},
	}

	search, err := client.SearchByIMDB(&ids)
	if err != nil {
		t.Fatal(err)
	}

	if len(search.Data) != 36 {
		t.Fatal("Result size mistmatch")
	}

}

func TestSearchByTag(t *testing.T) {

	search, err := client.SearchByTag("heroess01e08.avi")
	if err != nil {
		t.Fatal(err)
	}

	if len(search.Data) != 24 {
		t.Fatal("Result size mistmatch")
	}
}

func TestSearchByQuery(t *testing.T) {
	qry := []opensub.SearchQueryReq{
		opensub.SearchQueryReq{
			Query:         "south park",
			SubLanguageID: "eng,sp",
			Season:        "1",
			Episode:       "1",
		},
	}

	search, err := client.SearchByQuery(&qry)
	if err != nil {
		t.Fatal(err)
	}
	if len(search.Data) != 9 {
		t.Fatal("Result size mistmatch")
	}
}

func TestGetSubLang(t *testing.T) {
	search, err := client.GetSubLanguages("eng")
	if err != nil {
		t.Fatal(err)
	}

	if len(search.Data) != 75 {
		t.Fatal("Result size mistmatch")
	}
}

func TestGetUserInfo(t *testing.T) {
	user, err := client.GetUserInfo()
	if err != nil {
		t.Fatal(err)
	}

	if user.Data.IDUser == "" {
		t.Fatal("Result size mistmatch")
	}
}
