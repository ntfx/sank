package opensub

import (
	"strconv"

	"github.com/kolo/xmlrpc"
)

// Client login information
type Client struct {
	UserAgent string
	Token     string
	UserName  string
	Password  string
	Language  string
	*xmlrpc.Client
}

type rpcLoginRes struct {
	Token  string `xmlrpc:"token"`
	Status string `xmlrpc:"status"`
}

type rpcBaseRes struct {
	Status  string                 `xmlrpc:"status"`
	Data    map[string]interface{} `xmlrpc:"data"`
	Seconds float32                `xmlrpc:"seconds"`
}

// SubHashRes return the subtitles found on the database
type SubHashRes struct {
	Status  string            `xmlrpc:"status"`
	Data    map[string]string `xmlrpc:"data"`
	Seconds float32           `xmlrpc:"seconds"`
}

// MovieHash is the result data from CheckMovieHash function
type MovieHash struct {
	MovieHash     string `xmlrpc:"moviehash" json:"movie_hash"`
	MovieImdbID   string `xmlrpc:"movieimdbid" json:"movie_imdb_id"`
	MovieName     string `xmlrpc:"moviename" json:"movie_name"`
	MovieYear     string `xmlrpc:"movieyear" json:"movie_year"`
	MovieKind     string `xmlrpc:"moviekind" json:"movie_kind"`
	SeriesSeason  string `xmlrpc:"serieseason" json:"series_season"`
	SeriesEpisode string `xmlrpc:"seriesepisode" json:"series_episode"`
	SeenCount     string `xmlrpc:"seecount" json:"seen_count"`
	SubCount      string `xmlrpc:"subcount" json:"sub_count"`
}

// MovieHashRes from CheckMovieHash
type MovieHashRes struct {
	Status      string               `xmlrpc:"status" json:"status"`
	Data        map[string]MovieHash `xmlrpc:"data" json:"data"`
	NoProcessed []interface{}        `xmlrpc:"not_processed" json:"no_processed"`
	Seconds     float32              `xmlrpc:"seconds" json:"seconds"`
}

// MovieHash2Res from CheckMovieHash2
type MovieHash2Res struct {
	Status  string                 `xmlrpc:"status"`
	Data    map[string][]MovieHash `xmlrpc:"data"`
	Seconds float32                `xmlrpc:"seconds"`
}

// SubtitlesDataRes response
type SubtitlesDataRes struct {
	Status  string         `xmlrpc:"status"`
	Data    []SubtitleData `xmlrpc:"data"`
	Seconds float32        `xmlrpc:"seconds"`
}

// SubtitleData information
type SubtitleData struct {
	SubtitleID string `xmlrpc:"idsubtitlefile"`
	Data       string `xmlrpc:"data"`
}

// SearchHashIDReq request
type SearchHashIDReq struct {
	Hash          string `xmlrpc:"moviehash"`
	Size          int64  `xmlrpc:"moviebytesize"`
	SubLanguageID string `xmlrpc:"sublanguageid"`
}

// SearchIMDBReq request
type SearchIMDBReq struct {
	ImdbID        string `xmlrpc:"imdbid"`
	SubLanguageID string `xmlrpc:"sublanguageid"`
	Season        string `xmlrpc:"season"`
	Episode       string `xmlrpc:"episode"`
	Tags          string `xmlrpc:"tags"`
}

// SearchSubtitleRes response
type SearchSubtitleRes struct {
	Status  string           `xmlrpc:"status"`
	Data    []SearchSubtitle `xmlrpc:"data"`
	Seconds float32          `xmlrpc:"seconds"`
}

// SearchQueryReq information
type SearchQueryReq struct {
	Query         string `xmlrpc:"query"`
	SubLanguageID string `xmlrpc:"sublanguageid"`
	Season        string `xmlrpc:"season"`
	Episode       string `xmlrpc:"episode"`
}

// SearchTagReq information
type SearchTagReq struct {
	Tag           string `xmlrpc:"tag"`
	ImdbID        string `xmlrpc:"imdbid"`
	SubLanguageID string `xmlrpc:"sublanguageid"`
}

// SubLanguageRes response
type SubLanguageRes struct {
	Status  string        `xmlrpc:"status"`
	Data    []SubLanguage `xmlrpc:"data"`
	Seconds float32       `xmlrpc:"seconds"`
}

// SubLanguage information
type SubLanguage struct {
	SubLanguageID string `xmlrpc:"SubLanguageID"`
	LanguageName  string `xmlrpc:"LanguageName"`
	ISO639        string `xmlrpc:"ISO639"`
}

// GetUserInfoRes response
type GetUserInfoRes struct {
	Status  string      `xmlrpc:"status"`
	Data    GetUserInfo `xmlrpc:"data"`
	Seconds float32     `xmlrpc:"seconds"`
}

// GetUserInfo information
type GetUserInfo struct {
	IDUser                string `xmlrpc:"IDUser"`
	UserNickName          string `xmlrpc:"UserNickName"`
	UserRank              string `xmlrpc:"UserRank"`
	UploadCnt             string `xmlrpc:"UploadCnt"`
	UserPreferedLanguages string `xmlrpc:"UserPreferedLanguages"`
	DownloadCnt           string `xmlrpc:"DownloadCnt"`
	UserWebLanguage       string `xmlrpc:"UserWebLanguage"`
}

// SearchSubtitle information
type SearchSubtitle struct {
	MatchedBy          string `xmlrpc:"MatchedBy"`
	IDSubMovieFile     string `xmlrpc:"IDSubMovieFile"`
	MovieHash          string `xmlrpc:"MovieHash"`
	MovieByteSize      string `xmlrpc:"MovieByteSize"`
	MovieTimeMS        string `xmlrpc:"MovieTimeMS"`
	IDSubtitleFile     string `xmlrpc:"IDSubtitleFile"`
	SubFileName        string `xmlrpc:"SubFileName"`
	SubActualCD        string `xmlrpc:"SubActualCD"`
	SubSize            string `xmlrpc:"SubSize"`
	SubHash            string `xmlrpc:"SubHash"`
	SubLastTS          string `xmlrpc:"SubLastTS"`
	IDSubtitle         string `xmlrpc:"IDSubtitle"`
	UserID             string `xmlrpc:"UserID"`
	SubLanguageID      string `xmlrpc:"SubLanguageID"`
	SubFormat          string `xmlrpc:"SubFormat"`
	SubSumCD           string `xmlrpc:"SubSumCD"`
	SubAuthorComment   string `xmlrpc:"SubAuthorComment"`
	SubAddDate         string `xmlrpc:"SubAddDate"`
	SubBad             string `xmlrpc:"SubBad"`
	SubRating          string `xmlrpc:"SubRating"`
	SubDownloadsCnt    string `xmlrpc:"SubDownloadsCnt"`
	MovieReleaseName   string `xmlrpc:"MovieReleaseName"`
	MovieFPS           string `xmlrpc:"MovieFPS"`
	IDMovie            string `xmlrpc:"IDMovie"`
	IDMovieImdb        string `xmlrpc:"IDMovieImdb"`
	MovieName          string `xmlrpc:"MovieName"`
	MovieNameEng       string `xmlrpc:"MovieNameEng"`
	MovieYear          string `xmlrpc:"MovieYear"`
	MovieImdbRating    string `xmlrpc:"MovieImdbRating"`
	SubFeatured        string `xmlrpc:"SubFeatured"`
	UserNickName       string `xmlrpc:"UserNickName"`
	ISO639             string `xmlrpc:"ISO639"`
	LanguageName       string `xmlrpc:"LanguageName"`
	SubComments        string `xmlrpc:"SubComments"`
	SubHearingImpaired string `xmlrpc:"SubHearingImpaired"`
	UserRank           string `xmlrpc:"UserRank"`
	SeriesSeason       string `xmlrpc:"SeriesSeason"`
	SeriesEpisode      string `xmlrpc:"SeriesEpisode"`
	MovieKind          string `xmlrpc:"MovieKind"`
	SubHD              string `xmlrpc:"SubHD"`
	SeriesIMDBParent   string `xmlrpc:"SeriesIMDBParent"`
	SubEncoding        string `xmlrpc:"SubEncoding"`
	SubDownloadLink    string `xmlrpc:"SubDownloadLink"`
	ZipDownloadLink    string `xmlrpc:"ZipDownloadLink"`
	SubtitlesLink      string `xmlrpc:"SubtitlesLink"`
	QueryNumber        string `xmlrpc:"QueryNumber"`
}

// SubtitlePreview response
type SubtitlePreviewRes struct {
	Status  string            `xmlrpc:"status"`
	Data    []SubtitlePreview `xmlrpc:"data"`
	Seconds float32           `xmlrpc:"seconds"`
}

// SubtitlePreview information
type SubtitlePreview struct {
	Contents string `xmlrpc:"contents"`
	Encoding string `xmlrpc:"encoding"`
}

// MoreDownloads return the index of the subtitles with more downloads count
func (sr *SearchSubtitleRes) MoreDownloads() (int, error) {
	best := 0
	bestIdx := -1
	for i, x := range sr.Data {
		subcnt, err := strconv.Atoi(x.SubDownloadsCnt)
		if err != nil {
			return -1, err
		}
		if subcnt > best {
			bestIdx = i
			best = subcnt
		}
	}

	if bestIdx < 0 {
		bestIdx = 0
	}

	return bestIdx, nil
}
