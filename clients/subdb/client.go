package subdb

import (
	"fmt"
	"net/http"
	"net/url"
	"os"

	"io/ioutil"

	"golang.org/x/net/proxy"
)

// APIurl is the SubDB url for the API
const APIurl = "http://api.thesubdb.com/"

// APIurlSandBox is the SubDB url for the sandbox API
const APIurlSandBox = "http://sandbox.thesubdb.com/"

// New create a new http client
func New(apiURL string, ua string) *Client {
	s := Client{
		UserAgent: ua,
		Client:    proxyAwareHTTPClient(),
	}

	return &s
}

func proxyAwareHTTPClient() *http.Client {
	// sane default
	var dialer proxy.Dialer
	// eh, I want the type to be proxy.Dialer but assigning proxy.Direct makes the type proxy.direct
	dialer = proxy.Direct
	proxyServer, isSet := os.LookupEnv("HTTP_PROXY")
	if isSet {
		proxyURL, err := url.Parse(proxyServer)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid proxy url %q\n", proxyURL)
		}
		dialer, err = proxy.FromURL(proxyURL, proxy.Direct)
	}

	// setup a http client
	httpTransport := &http.Transport{}
	httpClient := &http.Client{Transport: httpTransport}
	httpTransport.Dial = dialer.Dial
	return httpClient
}

// Languages return available languages
func (c *Client) Languages() ([]byte, error) {
	return c.doRequest("action=languages")
}

// Search look for subtitles using the movie hash
func (c *Client) Search(hash string) ([]byte, error) {
	return c.doRequest("action=search&versions&hash=" + hash)
}

// Download the subtitles for the specified movie hash
func (c *Client) Download(hash string, lang string) ([]byte, error) {
	return c.doRequest("action=download&hash=" + hash + "&language=" + lang)
}

func (c *Client) doRequest(action string) ([]byte, error) {
	req, err := http.NewRequest("GET", APIurl+"?"+action, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("User-Agent", c.UserAgent)

	res, err := c.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("Request error: %s", res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
