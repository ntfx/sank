package subdb

import "net/http"

// Client information
type Client struct {
	UserAgent string
	Client    *http.Client
}
