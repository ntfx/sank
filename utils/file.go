package utils

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"golang.org/x/text/encoding/htmlindex"
)

const (
	chunkSize = 65536 // 64k
)

// UnBox will get a string gziped and base64 encoded and return a clean
// bytestream.
// *indData* holds the base64 string and strEncoderName is an optional value
// that is the source string encoding, line utf-8, latin1, cp1252, etc.
func UnBox(inData string, strEncoderName string) ([]byte, error) {

	b64decoded := base64.NewDecoder(base64.StdEncoding, strings.NewReader(inData))
	zReader, err := gzip.NewReader(b64decoded)
	if err != nil {
		return nil, fmt.Errorf("initializing decompressor: %s", err.Error())
	}

	var iReader io.Reader
	if strEncoderName != "" {
		strEncoder, err := htmlindex.Get(strEncoderName)
		if err != nil {
			return nil, fmt.Errorf("string encoder: %s", err.Error())
		}
		iReader = strEncoder.NewDecoder().Reader(zReader)
	} else {
		iReader = zReader
	}

	outData, err := ioutil.ReadAll(iReader)
	if err != nil {
		return nil, fmt.Errorf("decompresing: %s", err.Error())
	}

	return outData, nil
}

// HashFile calculate a movie hash based on OpenSubtitles Algorithm
// http://trac.opensubtitles.org/projects/opensubtitles/wiki/HashSourceCodes
func HashFile(filename string) (uint64, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return 0, err
	}
	if stat.Size() < chunkSize {
		return 0, fmt.Errorf("HashFile: File is too small")
	}

	buf := make([]byte, chunkSize*2)
	// Read first 64k
	err = readChunk(file, 0, buf[:chunkSize])
	if err != nil {
		return 0, err
	}
	// Read last 64k
	err = readChunk(file, stat.Size()-chunkSize, buf[chunkSize:])
	if err != nil {
		return 0, err
	}

	// Convert to uint64, and sum.
	var nums [(chunkSize * 2) / 8]uint64
	reader := bytes.NewReader(buf)
	err = binary.Read(reader, binary.LittleEndian, &nums)
	if err != nil {
		return 0, err
	}
	hash := uint64(0)
	for _, num := range nums {
		hash += num
	}

	return hash + uint64(stat.Size()), nil
}

// Read a chunk of a file at `offset` so as to fill `buf`.
func readChunk(file *os.File, offset int64, buf []byte) (err error) {
	n, err := file.ReadAt(buf, offset)
	if err != nil {
		return
	}
	if n != chunkSize {
		return fmt.Errorf("Invalid read %v", n)
	}
	return
}

// FileSize return the file size from a given filename
func FileSize(filename string) (int64, error) {
	stat, err := os.Stat(filename)
	if err != nil {
		return 0, err
	}
	return stat.Size(), nil
}

// SdbMovieHash is the hash format used on TheSubDB.com
func SdbMovieHash(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return "", err
	}
	if stat.Size() < chunkSize {
		return "", fmt.Errorf("HashFile: File is too small")
	}

	if stat.Size() < chunkSize {
		return "", fmt.Errorf("HashFile: File is too small")
	}

	buf := make([]byte, chunkSize*2)
	// Read first 64k
	err = readChunk(file, 0, buf[:chunkSize])
	if err != nil {
		return "", err
	}
	// Read last 64k
	err = readChunk(file, stat.Size()-chunkSize, buf[chunkSize:])
	if err != nil {
		return "", err
	}

	hash := md5.New()
	ioBuf := bytes.NewBuffer(buf)
	if _, err := io.Copy(hash, ioBuf); err != nil {
		return "", err
	}

	return fmt.Sprintf("%016x", hash.Sum(nil)), nil
}
